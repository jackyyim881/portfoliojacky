import React from "react";
import { Container, Row, Tab, Col, Nav } from "react-bootstrap";
import projImg3 from "../assets/img/project-img3.png";
import projImg1 from "../assets/img/project-img2.png";
import projImg2 from "../assets/img/project-img1.png";
import colorSharp2 from "../assets/img/color-sharp2.png";
import { ProjectCard } from "./ProjectCard";
export const Projects = () => {
  const projects = [
    {
      title: "business Startup",
      description: "Design & Development",
      imgUrl: projImg1,
    },
    {
      title: "business Startup",
      description: "Design & Development",
      imgUrl: projImg2,
    },
    {
      title: "business Startup",
      description: "Design & Development",
      imgUrl: projImg3,
    },
    {
      title: "business Startup",
      description: "Design & Development",
      imgUrl: projImg1,
    },
    {
      title: "business Startup",
      description: "Design & Development",
      imgUrl: projImg2,
    },
    {
      title: "business Startup",
      description: "Design & Development",
      imgUrl: projImg3,
    },
  ];

  return (
    <section className="project" id="projects">
      <Container>
        <Row>
          <Col>
            <h2>Project</h2>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Hic</p>
            <Tab.Container id="project-tab" defaultActiveKey="first">
              <Nav
                variant="pills"
                className="nav-pills mb-5 justify-content-center align-items"
              >
                <Nav.Item>
                  <Nav.Link eventKey="first">Tab One</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="second">Tab Two</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="third">Tab Three</Nav.Link>
                </Nav.Item>
              </Nav>
              <Tab.Content>
                <Tab.Pane eventKey="first">
                  <Row>
                    {projects.map((project, index) => {
                      return <ProjectCard key={index} {...project} />;
                    })}
                  </Row>
                </Tab.Pane>
                <Tab.Pane eventKey="second" className="">
                  <div class="box glowing"></div>
                </Tab.Pane>
                <Tab.Pane eventKey="third" className="">
                  <div class="box glowing"></div>
                </Tab.Pane>
              </Tab.Content>
            </Tab.Container>
          </Col>
        </Row>
      </Container>
      <img className="background-image-right" src={colorSharp2} alt="Image" />
    </section>
  );
};

export default Projects;
